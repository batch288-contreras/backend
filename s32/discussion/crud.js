let http = require('http');

// Mock database
let directory = [
  {
    "name": "Brandon",
    "email": "brandon@gmail.com"
  },
  {
    "name": "Jobert",
    "email": "jobert@gmail.com"
  }
];

http.createServer(function(request, response) {
  if (request.url == "/users" && request.method == "GET") {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify(directory));
    response.end();
  }

  // POST method
  if (request.url == "/addUser" && request.method == "POST") {
    let requestBody = "";
    request.on('data', function(data) {
      requestBody += data;
    });

    request.on('end', function() {
      requestBody = JSON.parse(requestBody);
      let newUser = {
        "name": requestBody.name,
        "email": requestBody.email
      };

      directory.push(newUser);
      console.log(directory);
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify(newUser));
      response.end();
    });
  }
}).listen(3000);

console.log('Server running at localhost:3000');
