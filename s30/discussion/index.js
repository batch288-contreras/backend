// aggregation mongodb

/*
	used to generate manipulated data and perform operations to create filtered results that
	helps analyzing data.
	-compare to doing crud operations

*/


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total:{ sum:"$stock"}}}
	])


db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  }
])



// the $group operator in terms of the property declare in the _id
db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      total: { $sum: "$stock" }
    }
  }
])


db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      total: { $sum: "$stock" }
    }
  }
])

//max operator
db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$supplier_id",
      sum: { $max: "$stock" }
    }
  }
])


db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$supplier_id",
      max: { $max: "stock"},
      sum: { $sum: "$stock" }
    }
  }
])


//Field projection with aggregation
/*
	-The $project operator can be used when aggregating data to exclude the returned result
*/


db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$supplier_id",
      max: { $max: "$stock" },
      sum: { $sum: "$stock" }
    }
  },
  {
    $project: {
      _id: 0
    }
  }
])


//match operator

db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $sum: "$stock" }
    }
  },
  {
    $sort: {
      total: 1
    }
  }
])


// sorting _id
db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$name",
      stocks: {"$stock" }
    }
  },
  {
    $sort: {
      total: {_id:1}
    }
  }
])



db.fruits.aggregate([
  {
    $match: {
      onSale: true
    }
  },
  {
    $group: {
      _id: "$name",
      stocks: { $sum: "$stock" }
    }
  },
  {
    $sort: {
      _id: -1
    }
  }
])


db.fruits.aggregate([
{$group: {
      _id: "$name",
      stocks: { $sum: "$stock" }
    }
  },
  {
    $sort: {
      _id: -1
    }
  }
])

// unwind operator
// look for the 

db.fruits.aggregate([
	{ $unwind: "$origin"}
	])




db.fruits.aggregate([
  { $unwind: "$origin" },
  {
    $group: {
      _id: "$origin",
      kinds: { $sum: 1 }
    }
  },
  { $sort: { _id: 1 } }
])
//the sum1 will count the number in documents

db.fruits.aggregate([
  { $unwind: "$origin" },
  {
    $group: {
      _id: "$origin",
      kinds: { $sum: 1 }
    }
  }
])