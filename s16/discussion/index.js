let x = 1397;
let y = 7881;

//Addition Operator (+)

let sum = x + y;


console.log("Result of Operator " + sum);


// sub op

let difference = y - x;

console.log("Result of subraction Operator: "+ difference);


// multi op

let product = x * y;
console.log("Result of multiplication: " + product);


// div op
let qoutient = y/x;
console.log("Result of division operator: " + qoutient);



//modulo operator %

let remaider = y % x;
console.log("Result of modulo operator: " + remaider);


// [Section] Assignment op
	//the assignment operator assigns the value or re assign the value to the variable

	let assignmentNumber = 8;

	assignmentNumber +=2;
	console.log(assignmentNumber);

	assignmentNumber+=3;

	console.log("Result of addition assignment operator " + assignmentNumber);


	// subtraction of assignment # 

	assignmentNumber -=2;;
	console.log("Result of subraction assignment operator " + assignmentNumber);



	// multi of assign #
	assignmentNumber*=3;
	console.log("Result of multiplication assignment operator " + assignmentNumber);


	// division of assign #

	assignmentNumber/=11;
	console.log("Result of division assignment operator " + assignmentNumber);


	//multiple operators and parentheses mdas rule
		// MDAS - multiplication - division - addition - subtraction

	let mdas = 1+ (2 -3) *(4 / 5);
	console.log(mdas);


	// increment and decrement
		// this is the operators that add or subract values by 1 and reassings the value of variable
	let z = 1;
	let increment = ++z;
	console.log("Result of pre-increment: " +increment);

	console.log("Result of pre increment: " + z);


	increment = z++;

	console.log("The result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);



	// pre decrement 
	 x= 1;
	let decrement = --x;
	console.log("The result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + x);


	decrement= x--;
	console.log("The result of post-increment: " + decrement);
	console.log("Result of post-increment: " + x);

	// [section] type of coercion

		// type coercion is automatic or implicit conversion of values from
	// one data type to another


	let numA ='10';
	let numB = 12;

	let coercion = numA + numB;

	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let noncoersion = numC+ numD;

	console.log(noncoersion);
	console.log(typeof noncoersion);

	// boolean
	let numE = true +1;
	console.log(numE);

	console.log(typeof numE);

	// [section] comparison operators

	let juan = 'juan';
	//	Equality Operator ==
		// checks operands are equal to have same content/value
		// returns a boolean value 	


	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	// case sensitive
	console.log('JUAN' == 'juan'); //false


	// Inequality op !=
	console.log(1!=1); //false


	// Strict Equality Operator ===
	// also checks data types

	console.log( 1 === 1); // true
	console.log(1 === '1'); //false
	console.log( 0 === false); //false
	console.log(juan === 'juan'); //false

	//strictly ineqaulity operator !==

	// vice versa of stict equlity operator

	console.log( 1 !== 1); // true
	console.log(1 !== '1'); //false
	console.log( 0 !== false); //false
	console.log(juan !== 'juan'); //false

	//[section] relational operators
	// checks whether one value is greater or less than other values.

	let a = 50;
	let b = 65;


	// 
	let isGreaterThan = a > b; //false
	//
	let isLessThan = a < b; // true
	//
	let isGTorEqual = a > b; //false
	//
	let isLTorEqual = a < b; //true

	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

	let numStr = "30";

	console.log(a > numStr);
	console.log(b <= numStr);


	let strNum = "twenty";
	console.log(b >= strNum); //false

	// [section] logical operators;

	let isLegalAge = true;
	let isRegistred = false;
	// 
	let allRequirementsMet = isLegalAge && isRegistred;

	console.log("Result of logical and operator: " + allRequirementsMet);

	// Logical OR Operator || - double pipe
	// atlease one is true

	let someRequirementsMet = isLegalAge || isRegistred;

	console.log("Result of logical or operator: " + someRequirementsMet);


	// Logical Not Operator (!- Exclamation Point)
	// Returns the opposite value

	let someRequirementsNotMet = !isRegistred;

	console.log("Result of logical NOT Operator: " + someRequirementsNotMet);