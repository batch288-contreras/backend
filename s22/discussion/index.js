// console.log("Good afternoon, Batch 288!");

//Array Methods
	// javaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	//Mutator Methods
	// Mutators methods are functions that "mutate" or change an array after they're created
	//These methods manipulate the original array performing various such as adding or removing elements.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			-Adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/

console.log("Current array: ");
console.log(fruits);

let fruitsLength = fruits.push('Mango');

console.log(fruitsLength);
console.log('Mutated array from push method: ');
console.log(fruits);

	// Pushing multple elements to an array

fruitsLength = fruits.push('Avocado', 'Guava');

console.log(fruitsLength);
console.log("Mutated array after pushing multiple elements:");
console.log(fruits);


	// pop()
	/*
		-removes the last element AND returns the removed element
		Syntax:
			arrayName.pop();
	*/

console.log("Current Array: ");
console.log(fruits);

let removedFruit = fruits.pop();

console.log(removedFruit);
console.log("Mutated Array from the pop method:");
console.log(fruits);

	//unshift()
	/*
		-it adds one or more elements at the beginning of an array AND it returns the update array length
		-Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB ' . . .)
	*/

console.log("Current Array:");
console.log(fruits);

fruitsLength = fruits.unshift('Lime', 'Banana');

console.log(fruitsLength);
console.log("Mutated array from unshift method: ");
console.log(fruits);

	//shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element.
		-syntax:
			arrayName.shift();
	*/

console.log("Current Array: ");
console.log(fruits);

fruits.shift();

console.log("Mutated array from the shift method: ");
console.log(fruits);


// splice


	/*


	*/



console.log("Current Array: ");
console.log(fruits);

	fruits.splice(1,2); //deletes a index 


	fruits.splice(0,0,"lime")

	fruits.splice(fruits.length -1, 1);

	console.log("Splice Array: ");
	console.log(fruits);

	// sort ()
	/*

	Rearranges the array elements in alphanumeric order

	-syntax:
	arrayName.sort();

	*/

	console.log("Current Array");
	console.log(fruits);

	fruits.sort();

	console.log("mutated array from the sort method");
	console.log(fruits);


	let countries = ['US','PH','SG','ID','FR','PH'];

	// indexOf()
	// returns the index number of the 1st matching element found in an array.
	// If no match was found, the result will be -1.
	// The search will be done from 1st element proceeding to the last element

	let firstIndex = countries.indexOf('PH');
	console.log(firstIndex);


	let invalidCountry = countries.indexOf('BR');
	console.log(invalidCountry);

	firstIndex = countries.indexOf('PH',2);
	console.log(firstIndex);


	// lastIndexOf()
	/*
		returns the index number of the last matching element found in an array
		the search will be done from last element proceeding to the first

		Syntax:
			arrayName.lastIndexOf(searrchvalue);
			arrayName.lastIndexOf(searrchvalue, starting index);
	
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf('BR');
	console.log(invalidCountry);

	invalidCountry = countries.lastIndexOf('PH',4);
	console.log(countries);

	// indexOf - 0-last
	// lastIndexOf - last - 0


	//
	// slice()
	/*
	-portions/slices elements from an array and return a new array
	-syntax
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, ending index)


	*/

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method:");
	console.log(slicedArrayA);


	// Slicing Off elements from specified index to another index;
	let spliceArrayB = countries.slice(2,4);
	console.log("Result from the slice method:");
	console.log(spliceArrayB)


	// slicing off elements starting from last element array
	let slicedArrayC = countries.slice(-3);
	console.log("Result from the slice method:");


	// make array to string
	let stringArray = countries.toString();
	console.log('Result from toString method');
	console.log(stringArray);
	console.log(typeof stringArray);

	// concat()

	//combines arrays to an array or elements and returns the combined result


	let tasksArrayA = ["drink HTML", "Eat Javascript"];
	let tasksArrayB = ["inhale Css", "Breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combinedTasks = tasksArrayA.concat("smell express", "throw react");
	console.log(combinedTasks);

	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	let users = ["john", "jacob", "micro"];
	console.log(users.join());

	console.log(users.join(''));
	console.log(users.join("-"));

	//iteration methods

	/*

	loops over / forEach()
	Similar to a for loop that iterates on each of array elements

	Syntax:
	arrayname.forEach(function(indivElement){statement};)

	*/

	console.log(allTasks);

	allTasks.forEach(function(task){
		console.log(task);
	});

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log(filteredTasks);

/*
//Map method = map()
	It returns new array with different values depending on the result of the function operation

*/

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(function(number){
	return number * 3;
})
console.log(numbers);
console.log(numberMap);

//every()
/*
	it will check if all elements in an array meet the given the condition
	-return true value if all elements meet the condition and false otherwise


	syntax:
	let/const resultArray = ArrayName.every(function(indivElement){
	return expression condition;
	})
*/

numbers = [1,2,3,4,5];
let allVAlid = numbers.every(function(number){
	return (number<3);
})

console.log(allVAlid);


//some()
// checks if at least one element in the array meets the given condition.
/*
	syntax:
	let/const resultArray = arrayName.some(function(indivElement){
	return expression/condition;
	})

*/

let someValid = numbers.some(function(number){
	return(number<2);
})

console.log(someValid);


// filter()
/*
	returns new array that contains elements which meets the given
	returns an empty array if no elements were found

	Syntax: let/const resultArray = arrayName.filter(
	function(indivElement){
		return expression/condition;
	})

*/

numbers = [1,2,3,4,5];
let filterValid = numbers.filter(function(number){
	return (number %2 ===0);
})

console.log(filterValid);


// includes()
/*
	// checks if the argument passed can be found in the array


*/


let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

// reduce()

/*
	evaluates elements from left to right and returns/reduce the array into single value


*/
numbers = [1,2,3,4,5];
// 1st parameter in the function will be the accumulator
// 2nd parameter in the function will be the currentValue
let reduceArray = numbers.reduce(function(x,y){
	console.log('Accumulator: ' + x)
	console.log('Current Value: ' + y)
	return x*y;
})

console.log(reduceArray);


let reduceArrayString = products.reduce(function(x,y){
	return (x+y);
})

console.log(reduceArrayString);