const express = require("express");

const taskControllers = require('../controllers/taskControllers.js');

//Contain all the endpoints of our application
const router = express.Router();


router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks);


//Parametrizer
// We are create a route using a delete method at url "/tasks/:id"
// the coln here is an identifier that helps to create a dynamic route which allows us to
// to supply information
router.delete("/:id", taskControllers.deleteTask)


// router for specific task
router.get("/:id", taskControllers.getSpecificTask)

router.put("/:id/complete", taskControllers.putUpdatedTask)

module.exports = router;