// show databases list of the db inside our cluster

// use dbName to use a specific database

// show collections to see the list of collections inside the db

//CRUD operation
/*

	- CRUD operation is the heart of any backend application.
	- mastering the crud operation is essential for any developer especially to those
	// who want to become backend developer


	// [Section] Insertion Document (Create)
	// Insert one document
		/*

			Since mongoDB deals with object it's structure for documents
			we can easily create them by providing objects in our method/operation

			Syntax:
			db.collectionName.insertone({
				object
			})


		*/

db.users.insertOne({
	firstName : "Jane",
	lastName : "Doe",
	age: 21,
	contact: {
		phone: "1234567890",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JAVA", "python"],
	department: "none"
})


db.users.insertOne({
	firstName : "Jane",
	lastName : "Doe",
	age: 21,
	contact: {
		phone: "1234567890",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JAVA", "python"],
	department: "none"
})

// Insert Many

/*

	syntax: dbcollectionName.insertMany([{object A}, {objectB}])


*/


db.users.insertMany([
{
	firstName : "Stephen",
	lastName : "Hawking",
	age : 76,
	contact: {
		phone: "8765123",
		email: "stephenhawking@gmial.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
},

{
	firstName : "Neil",
	lastName : "Armstrong",
	age : 82,
	contact: {
		phone: "131233",
		email: "neilarmstrong@gmial.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}

])


//[Section] Finding documents 

/* Syntax:
		db.collectionName.find();
		db.collectionName.find({field:value});

	

*/


db.users.find();
// shows the value of the db

db.users.find({firstName: "Stephen"});
// find single value


// find the id
db.users.find({
	"_id" : ObjectId("646c557e69ea4cd67baadb8b")
})


db.users.find({ firstName:"Neil",age: 82});


db.users.find({firstName: "Jane"});




db.users.find({contact:{phone: "1234567890",
		email: "janedoe@gmail.com" }});

// error db.users.find("contact.phone": "1234567890")

// [Section] Updating documents


db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});


// updateOne method

/*
	Syntax:
	db.collectionName.updateOne({ criteria,
	{$set: {field:value}}});

*/
	db.users.updateOne(
	{ firstName: "test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "none"
		}
	}
);

db.users.updateOne(
	{firstName: "Jane"},
	{

	$set: {
		lastName: "Edited"
		}
	}
)

//Updating multiple documents
/*
	Syntax:
	db.collectionName.updateMany(
	{criteria},
	$set:{
		{field:value}
		}
	}
)
*/

db.users.updateMany(
	{department: "none"},
	{

	$set: {
		department: "HR"
		}
	}
	)

// Replace One
/*
	Syntax: db.collectionName.replaceOne(
		{criteria},
		{	
			object
		}		
	)
*/

db.users.insertOne({firstName:"test"});

db.users.replaceOne(
	{firstName: "test"},
	{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{},
			courses: [],
			department: "Operations"
		}
	)
// [Section] Deleting Documents
	/*
		db.collectionName.deleteOne({criteria});
	*/


db.users.deleteOne({firstName: "Bill"});

// delete multe document
	/*
		db.collectionName.deleteMany({criteria})
	*/

db.users.deleteMany({firstName: "Jane"});

db.users.deleteMany({});
