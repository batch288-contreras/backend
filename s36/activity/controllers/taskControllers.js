const Task = require('../models/task.js');

/*Controllers*/

//This controller will get/retrieve all the document from the tasks collections

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

//Create a controller that will a data in our database
module.exports.addTasks = (request, response) => {

	Task.findOne({"name" : request.body.name})
	.then(result => {

		if(result !== null){
			return response.send("Duplicate Task");
		}else{
			let newTask = new Task({
				"name": request.body.name 
			})

			newTask.save();

			return response.send("New task created!");
		}
	}).catch(error => response.send(error))
}

module.exports.deleteTask = (request, response) =>{
	console.log(request.params.id);

	let taskToBeDeleted = request.params.id;

	//In mongoose, we have the findByIdAndRemove method that will look for a document with the same id provided from the URL and remove/delete the document from the MongodDB
	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted!`)
	}).catch(error => response.send(error))

}


// Specific Task module

module.exports.getSpecificTask = (request, response) => {
  const taskId = request.params.id;
  Task.findById(taskId)
    .then(result => {
      if (result) {
        return response.send(result);
      } else {
        return response.send(`Task with ID ${taskId} not found.`);
      }
    })
    .catch(error => {
      return response.send(error);
    });
};

// Updated task
module.exports.putUpdatedTask = (request, response) => {
	const taskId = request.params.id;
  	const updatedTaskStatus = request.body;

	Task.findByIdAndUpdate(taskId, updatedTaskStatus, { new: true })
    .then(result => {
      if (result) {
        return response.send(result);
      } else {
        return response.send(`Task with ID ${taskId} not found.`);
      }
    })
    .catch(error => {
      return response.send(error);
    });
};