let http = require('http');

let app = http.createServer(function(request, response) {

	if (request.method === "GET") {
		if (request.url === "/") {
			response.writeHead(200, { 'Content-Type': 'text/plain' });
			response.end('Welcome to booking system');
		} else if (request.url === "/profile") {
			response.writeHead(200, { 'Content-Type': 'text/plain' });
			response.end('Welcome to your profile');
		}
		else if (request.url === "/courses") {
			response.writeHead(200, { 'Content-Type': 'text/plain' });
			response.end(`Here's our courses available`);
		}

	}

  //post method
	if (request.method === "POST" && request.url === "/addCourse")
	{ 
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Add course to our resources');
	}

  // put method
	if (request.method === "PUT" && request.url === "/updateCourse")
	{
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Update a course to our resources');
	}


 // delete method
	if (request.method === "DELETE" && request.url === "/archiveCourse") {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Archive courses to our resources');
	}



}).listen(4000);

console.log('Server running at http://localhost:4000');





//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
