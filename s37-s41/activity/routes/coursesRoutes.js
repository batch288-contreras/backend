const express = require('express');
const coursesControllers = require('../controllers/coursesControllers.js');
const router = express.Router();
const auth = require("../auth.js")

router.get('/activeCourses', coursesControllers.getActiveCourses);
router.get('/:courseId', coursesControllers.getCourse);

router.get('/inactiveCourses', coursesControllers.getInactiveCourses);
router.post('/addCourse', auth.verify, coursesControllers.addCourse);
router.get('/', auth.verify, coursesControllers.getAllCourses);



router.patch('/:courseId', auth.verify, coursesControllers.updateCourse);
router.patch('/:courseId/archive', auth.verify, coursesControllers.archiveCourse);



module.exports = router;



