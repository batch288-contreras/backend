// Single line comment

/*
	Multi Line Comment
	This is a Comment



*/


// [Section] Syntax, Statements and Comments
// States in programming, these are the instructions that we tell the computer to perform
// JavaScript Statement usually it ends with semicolon(;)
// Semicolons are not req in JS, but we will use it to help us prepare for ther stict 
// All lines/blocks of code should be written in specific manner



// [section] Variables
// It is used to contain/store data.
// Any information that is used by an application is stored in memory
// When we create variables, certain portion  of defvice memory is given a "name" that we call variables

// Declaring Variables
// Declaring vairiables- tells our devices that a variable name is created and ready to store data


	//Syntax:
		// let/const - variableName



// by default if you declare a variable and did not inialize a value else undefined.

// console.log() -
let productName = 'desktop-computer';
let productPrice = 18999;
const interest = 3.539;

console.log(productName)
console.log(productPrice)

// Reassignint Variable Values
// changing its previous value into another value


productName = 'Laptop';


// let/const local/gloabl scope

// scope essentially means where these variables are available or accesible for use

let outervariable ="hello";
let globalVariable="innervariable";
	{
		let innervariable ="hello again";

	}

	console.log(globalVariable)
// multiple varialbes to be consoled in one line

let productCode="DC01"; let productBrand ="Dell";

console.log(productCode, productBrand)
// reserved keywords cannot be used as a variable name with function

// const let = "basic";


// String = text



let country = 'Philippines';
let province = 'Metro Manila';
		// Contenation
			//  +


let fullAddress = province + country;

console.log(fullAddress);

let greetings ='I live in the ' + country;

console.log(greetings);

let mailAddress ='Metro Manila\n \nPhilippines'
// \n = new line
console.log(mailAddress)


let message = "John \'s employee went home early.";

console.log(message)


// Numbers
let headcount=26;
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

// combine text and strings is always become string


console.log("Jon's grade last quarter is "+grade);


//Boolean - true or false

//Boolean value 

let isMarried = false;
let isGoodConduct = true;


console.log("isMarried: " + isMarried);
console.log(isGoodConduct);


//Arrays
//Arrays are special kind of data that's used to store multiple related values.

// Can store differefent data types but is normally to used to store similar data type


//syntax: for array

//let cons ArrayName = [elementaA], [ElementB], [elementC];

let grades =[98.9, 94.5, 34, true];
console.log(grades)


//objects
// syntax
	// let/const objectname={
//     	propertyA: valueA,
//		propertyB: valueB
// }

let person ={
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contanct: ["+63917 123 4567", "8123 4567"],
	address:{
		houseNumber: '345',
		city: 'Manila',
	}
}

console.log(typeof person);

console.log(typeof grades);


// Note: Array is a special type of object with methods and function to manipulate

// Constat object and arrays 


const anime =["One piece", "One Punch Man", "Attack on Titan"];


anime [2] = "basic"
console.log(anime);



// null = no assign value

let spouse = null;

