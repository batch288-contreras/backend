// Use the "require" directive to load Node.js modules
// A module is a software component or part of a program that contains
// one or more routines

// The "http module" lets node.js transfer data using the hyper text transfer protocol

//In that way, HTTP is a protocol that allows the fetching of resources such as HTML documents


//Clients (browser) and Servers (Node Js/ Express Js applications communicate by exchanging individual message)

// These messages are sent by the clients, usually a web browser and called "request"

//The messages sent by the server as an answer called "response"

let http = require("http");


// using this modules createServer() method, we can create an HTTP server that listens to request on a 
// specified port and gives responses back to client
// A port is virtual point where network connections starts and end.

// the http module has a createServer() method that accepts a function as an arguement and allows
// for creation of a server

// The argument passed in the function are request and reponste
// objects (data types) that contains methods that allows us to receive request from
// the client and send response back to it

// The server will be assigned to port 4000 vita the listen(4000) method
// where the server will listen to any request that are sent to it eventually 
// communication with our server


	// Use the writehead() method to:
	// Set a status code for the response - a 200 means OK
	// Set content-type of the response as plain text message


	
	// Send the response with text content 'Hello World'




const server = http.createServer(function (request, response) {
  response.writeHead(200, { 'Content-Type': 'text/plain' });
  response.end('Hello World?!');
});

server.listen(4005, function () {
  console.log('Server running at http://localhost:8000');
});
