/*

[SECTION] JSON Objects
	JSON stands for JavaScript Object Notation
	JSON is also used in other programming languanges hence the name
	JavaScript Object Notation
	.json file ,JSON file

*/

//JavaScript objects are not to be confused with JSON.

//Serialiation is the process of converting data into series of bytes for easier tranmission/
// A byte is ubit of data that is eight binary digits (1,0) that is used to represents
//a character (letter, numbers, typographic, symbols.)


/*
	Syntax: {
		"propertyA" = "valueA"
		"propertyB" = "valueB"
	}
*/

// [Section] JSON Arrays
/*
		"cities" : [
		{" city": "Quezon City", "province", "Metro Manila", "country": "Philippines"}
		]

*/


// [Section] JSON Methods
		// The JSON object contains methods for pasrsing and converting data into stringofield JSON.

		/*
				Converting Data into Stringifiled JSON
				Stringifield JSON is a JavaScript object converted into a string to be used
				in other funtions of JavaScript application
				They are coomonly used in HTTP request where information is required to be sent and receive
				in a stringifield version
				// Requests are an importnant type of programmign whre an application communicates with a backend application
				// to perform diffent task such as getting/creating data in a database					
		*/

		let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

console.log(batchesArr);

console.log('Result from stringify method');

console.log(JSON.stringify(batchesArr));


// Using stringify method with variables
/*

		JSON.strinfify({
			propertyA: variableA,
			propertyB: variableB
		})
*/


/*
//user details
let fristName = prompt('What is your first name: ');
let lastName = prompt(`What is your last name: `);
let age = prompt(`What is your aga? `);
let address = {
	city: prompt(`which city do you live in? `),
	country: prompt(`Which country does your ciry address belongs to?`)
};



let otherData = JSON.stringify({
	fristName: fristName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);

*/
let batchesJSON = '[{"batchNumber": "1999"}, {"batchName": "BatchY"}]';
console.log(batchesJSON);
console.log("result from parse method: ");
console.log(JSON.parse(batchesJSON));


let exampleObject = '{"name": "Chris", "age": 16, "isTeenager": false}';

let exampleParse = JSON.parse(exampleObject);
console.log(typeof exampleParse.name);
console.log(typeof exampleParse.age);
console.log(typeof exampleParse.isTeenager);