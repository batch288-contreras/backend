// //Note: don't add a semicolon at the end of then().
// //Fetch answers must be inside the await () for each function.
// //Each function will be used to check the fetch answers.
// //Don't console log the result/json, return it.


// // Getting all to do list item
async function getAllToDo(){

   return await (

      fetchData('https://jsonplaceholder.typicode.com/posts',{
         method: "Get",
         headers: {
            'Content-Type': 'application/json'
         }
      })
      .then((response)=>response.json())
      .then((json)=>{
         return json.map(item => item.title)
      })
   )
 }




// [Section] Getting a specific to do list item
async function getSingleToDo(){

    return await (

   fetch('https://jsonplaceholder.typicode.com/posts/1')
   .then(response => response.json())
   .then(json =>(json))
   
 )
}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

   fetch('https://jsonplaceholder.typicode.com/posts', {

      method: "post",
      headers: {
         'Content-type' : 'application/json'
      },
      body: JSON.stringify({
            
               createToDoItem: 'New item',
               
            })
   })
   .then(response => response.json())
   .then(json => (json))


   );

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

   fetch('https://jsonplaceholder.typicode.com/posts/1', {

      method: "put",
      headers: {
         'Content-type' : 'application/json'
      },
      body: JSON.stringify({
               title: 'Updated Post',
               description: 'new description',
               status: 'newstatus',
               dataCompleted: 'new datecompleted',
               userId: 'new userId'

            })
   })
   .then(response => response.json())
   .then(json => (json))



   );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

   fetch('https://jsonplaceholder.typicode.com/posts/1', {
      method : "delete"})
   .then(response => response.json())
   .then(json => (json))


   );

}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}